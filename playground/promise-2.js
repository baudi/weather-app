const request = require('request');

var geocodeAdress = (address) => {
	return new Promise((resolve, reject)=>{
		var encodeAddress = encodeURIComponent(address);
		request({
			url: `http://maps.googleapis.com/maps/api/geocode/json?address=${encodeAddress}`,
			json: true
		}, (error, response, body) => {
			if (error) {
				reject('Unable to connect to Google server');
			}else if (body.status === 'ZERO_RESULTS') {
				reject('Unable to find that address');
			}else if (body.status === 'OK') {
				resolve({
					address: body.results[0].formatted_address,
					latitude: body.results[0].geometry.location.lat,
					longitude: body.results[0].geometry.location.lng
				});
			}
		});
	});
};

geocodeAdress('19146').then((location)=>{
	console.log(JSON.stringify(location, undefined, 2));
}, (errorMessage)=>{
	console.log(errorMessage);
})