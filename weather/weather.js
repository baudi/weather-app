const request = require('request');

var getWeather = (lat, lng, callback) => {
	request({
		url: `https://api.forecast.io/forecast/b78900d6b9b5aa116840f8b8b91b6974/${lat},${lng}`,
		json: true
	}, (error, response, body) => {
		if (!error && response.statusCode === 200) {
			callback(undefined, {
				temperature: body.currently.temperature,
				apparentTemperature: body.currently.apparentTemperature
			});
		}else {
			callback('Unable to fetch weather');
		}
	});
}

module.exports = {getWeather};